table! {
    use diesel::sql_types::*;
    use super::Currency;

    accounts (id) {
        id -> Uuid,
        user_id -> Uuid,
        name -> Varchar,
        created_at -> Timestamp,
        value -> Int8,
        currency -> Currency,
        priority -> Int2,
    }
}

table! {
    use diesel::sql_types::*;
    use super::Currency;

    categories (id) {
        id -> Uuid,
        user_id -> Uuid,
        name -> Varchar,
    }
}

table! {
    use diesel::sql_types::*;
    use super::Currency;

    envelopes (id) {
        id -> Uuid,
        user_id -> Uuid,
        name -> Varchar,
        created_at -> Timestamp,
        value -> Int8,
        currency -> Currency,
        category_id -> Uuid,
    }
}

table! {
    use diesel::sql_types::*;
    use super::Currency;

    transactions (id) {
        id -> Uuid,
        user_id -> Uuid,
        account_id -> Uuid,
        envelope_id -> Uuid,
        notes -> Varchar,
        value -> Int8,
        tdate -> Date,
    }
}

table! {
    use diesel::sql_types::*;
    use super::Currency;

    users (id) {
        id -> Uuid,
        name -> Varchar,
        email -> Varchar,
        phc -> Varchar,
        confirmed -> Bool,
        user_type -> Int2,
        primary_currency -> Currency,
        joined -> Timestamp,
        last_login -> Nullable<Timestamp>,
    }
}

joinable!(accounts -> users (user_id));
joinable!(categories -> users (user_id));
joinable!(envelopes -> categories (category_id));
joinable!(envelopes -> users (user_id));
joinable!(transactions -> accounts (account_id));
joinable!(transactions -> envelopes (envelope_id));
joinable!(transactions -> users (user_id));

allow_tables_to_appear_in_same_query!(
    accounts,
    categories,
    envelopes,
    transactions,
    users,
);
