-- This file should undo anything in `up.sql`
ALTER TABLE transactions DROP COLUMN tdate;
ALTER TABLE transactions ADD COLUMN time timestamp NOT NULL DEFAULT '2019-01-01 0:0:0';
