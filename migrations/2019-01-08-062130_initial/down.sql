-- This file should undo anything in `up.sql`
DROP TABLE transactions;
DROP TABLE envelopes;
DROP TABLE accounts;
DROP TABLE users;
DROP TYPE currency;
