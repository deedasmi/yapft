-- Your SQL goes here
CREATE TYPE currency AS ENUM ('USD', 'JPY');

CREATE TABLE users (
  id UUID PRIMARY KEY,
  name VARCHAR NOT NULL,
  email VARCHAR UNIQUE NOT NULL,
  phc VARCHAR NOT NULL,
  confirmed BOOLEAN NOT NULL DEFAULT FALSE,
  user_type SMALLINT NOT NULL DEFAULT 0,
  primary_currency currency NOT NULL DEFAULT 'USD',
  joined TIMESTAMP NOT NULL DEFAULT NOW(),
  last_login TIMESTAMP
);

CREATE TABLE envelopes (
  id UUID PRIMARY KEY,
  user_id UUID NOT NULL REFERENCES users(id),
  name VARCHAR NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  value BIGINT NOT NULL DEFAULT 0,
  currency CURRENCY NOT NULL
);

CREATE TABLE accounts (
  id UUID PRIMARY KEY,
  user_id UUID NOT NULL REFERENCES users(id),
  name VARCHAR NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  value BIGINT NOT NULL DEFAULT 0,
  currency CURRENCY NOT NULL
);

CREATE TABLE transactions (
  id UUID PRIMARY KEY,
  user_id UUID NOT NULL REFERENCES users(id),
  account_id UUID NOT NULL REFERENCES accounts(id),
  envelope_id UUID NOT NULL REFERENCES envelopes(id),
  payee VARCHAR NOT NULL, 
  value BIGINT NOT NULL
);

ALTER TABLE envelopes ADD UNIQUE(user_id, name);
ALTER TABLE accounts ADD UNIQUE(user_id, name);
