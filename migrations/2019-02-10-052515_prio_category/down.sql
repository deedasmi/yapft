-- This file should undo anything in `up.sql`
ALTER TABLE envelopes DROP COLUMN category;
ALTER TABLE accounts DROP COLUMN priority;
ALTER TABLE transactions DROP COLUMN time;
