-- This file should undo anything in `up.sql`
ALTER TABLE envelopes DROP CONSTRAINT category_fkey;
ALTER TABLE envelopes DROP COLUMN category_id;
DROP TABLE categories;
ALTER TABLE envelopes ADD COLUMN category VARCHAR NOT NULL DEFAULT 'Uncategoized';
ALTER TABLE transactions RENAME COLUMN notes TO payee;
