-- Your SQL goes here
ALTER TABLE envelopes DROP COLUMN category;
CREATE TABLE categories (
    id UUID PRIMARY KEY, 
    user_id UUID NOT NULL REFERENCES users(id),
    name VARCHAR NOT NULL
);
ALTER TABLE categories ADD UNIQUE(user_id, name);
ALTER TABLE envelopes ADD COLUMN category_id UUID NOT NULL;
ALTER TABLE envelopes ADD CONSTRAINT category_fkey FOREIGN KEY (category_id) REFERENCES categories (id);
ALTER TABLE transactions RENAME COLUMN payee TO notes;
