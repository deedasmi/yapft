table! {
    use diesel::sql_types::*;
    use super::Currency;

    accounts (id) {
        id -> Uuid,
        user_id -> Uuid,
        name -> Varchar,
        created_at -> Nullable<Timestamp>,
        value -> Int4,
        currency -> Currency,
    }
}

table! {
    use diesel::sql_types::*;
    use super::Currency;

    envelopes (id) {
        id -> Uuid,
        user_id -> Uuid,
        name -> Varchar,
        created_at -> Timestamp,
        value -> Int4,
        currency -> Currency,
    }
}

table! {
    use diesel::sql_types::*;
    use super::Currency;

    transactions (id) {
        id -> Uuid,
        user_id -> Uuid,
        from_account_id -> Uuid,
        to_account_id -> Uuid,
        envelope_id -> Nullable<Uuid>,
        value -> Int4,
        currency -> Currency,
    }
}

table! {
    use diesel::sql_types::*;
    use super::Currency;

    users (id) {
        id -> Uuid,
        name -> Varchar,
        email -> Varchar,
        phc -> Varchar,
        confirmed -> Bool,
        user_type -> Int2,
        primary_currency -> Currency,
        joined -> Timestamp,
        last_login -> Nullable<Timestamp>,
    }
}

joinable!(accounts -> users (user_id));
joinable!(envelopes -> users (user_id));
joinable!(transactions -> envelopes (envelope_id));
joinable!(transactions -> users (user_id));

allow_tables_to_appear_in_same_query!(
    accounts,
    envelopes,
    transactions,
    users,
);
