#![feature(proc_macro_hygiene, decl_macro)]
#![warn(clippy::all)]

#[macro_use]
extern crate diesel;

#[macro_use]
extern crate rocket;

#[macro_use]
extern crate rocket_contrib;

#[macro_use]
extern crate log;

#[macro_use]
extern crate diesel_migrations;

embed_migrations!("./migrations");

pub mod auth;
pub mod forms;
pub mod models;
mod money;
mod routes;
#[cfg_attr(tarpaulin, skip)]
pub mod schema;
mod templates;
use rocket::response::NamedFile;
use rocket_contrib::serve::StaticFiles;

fn main() {
    rocket().launch();
}

pub fn rocket() -> rocket::Rocket {
    rocket::ignite()
        .attach(PgConn::fairing())
        .attach(rocket::fairing::AdHoc::on_attach(
            "Run Main Database Migrations",
            |rocket| {
                let conn = PgConn::get_one(&rocket).expect("database connection");
                match embedded_migrations::run(&*conn) {
                    Ok(()) => Ok(rocket),
                    Err(e) => {
                        error!("Failed to run database migrations: {:?}", e);
                        Err(rocket)
                    },
                }
            },
        ))
        .mount("/static/", StaticFiles::from("static"))
        .mount("/", routes::get_routes())
        .mount("/", routes![favicon])
}

#[get("/favicon.ico")]
pub fn favicon() -> Option<NamedFile> {
    NamedFile::open("static/favicon.ico").ok()
}

#[database("pg_db")]
pub struct PgConn(diesel::PgConnection);

#[cfg(test)]
mod tests;
