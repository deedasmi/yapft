use super::rocket as get_rocket;
use crate::models::{accounts::Account, envelopes::PrettyEnvelope};
use rocket::{
    http::{ContentType, Cookie, Status},
    local::Client,
};
use serde::de::DeserializeOwned;

#[test]
fn basic_pages() {
    let r = get_rocket();
    let client = Client::new(r).expect("valid rocket instance");
    let req = client.get("/");
    let response = req.dispatch();
    assert_eq!(response.status(), Status::Ok);
    let req = client.get("/envelopes");
    let response = req.dispatch();
    assert_eq!(response.status(), Status::NotFound);
}

#[test]
fn account_crud() {
    let r = get_rocket();
    let client = Client::new(r).unwrap();
    create_new_user(&client);

    let resp = new_account(&client, "test");
    let _ = new_account(&client, "test2");
    assert!(flash_success(resp.cookies()));

    // Change priority
    let req = client
        .post("/accounts")
        .body("name=test&currency=USD&priority=10")
        .header(ContentType::Form);
    req.dispatch();
    let req = client.get("/api/accounts/test/");
    let resp = req.dispatch();
    assert_eq!(resp_parse::<Account>(resp).priority, 10,);

    // Number of accounts
    let req = client.get("/api/accounts");
    let resp = req.dispatch();
    assert_eq!(resp_parse::<Vec<Account>>(resp).len(), 2);

    // Normal delete
    let req = client.delete("/api/accounts/test");
    let resp = req.dispatch();
    assert!(flash_success(resp.cookies()));
}

#[test]
fn envelope_crud() {
    let r = get_rocket();
    let client = Client::new(r).unwrap();
    create_new_user(&client);
    new_envelope(&client, "keep");

    // Normal create
    let resp = new_envelope(&client, "test");
    assert!(flash_success(resp.cookies()));

    // Change Category
    let req = client
        .post("/envelopes")
        .body("name=test&currency=USD&category=boom")
        .header(ContentType::Form);
    let resp = req.dispatch();
    assert!(flash_success(resp.cookies()));
    let req = client.get("/api/envelopes/test");
    let resp = req.dispatch();
    assert_eq!(resp_parse::<PrettyEnvelope>(resp).category, "boom");

    // Normal delete
    let req = client.delete("/api/envelopes/test");
    let resp = req.dispatch();
    assert!(flash_success(resp.cookies()));

    // Delete uncategorized
    let req = client.delete("/envelopes/Uncategorized");
    let resp = req.dispatch();
    assert_eq!(flash_success(resp.cookies()), false);

    // Still two
    let req = client.get("/api/envelopes/");
    let resp = req.dispatch();
    assert_eq!(resp_parse::<Vec<PrettyEnvelope>>(resp).len(), 2);
}

#[test]
fn transactions() {
    let r = get_rocket();
    let client = Client::new(r).unwrap();
    create_new_user(&client);

    new_account(&client, "test");
    new_account(&client, "test2");
    new_envelope(&client, "test");

    // Negative should be ignored
    let resp = post(
        &client,
        "/transactions",
        "account=test&notes=test&envelope=test&value=-500&income=Income&date=2019-01-01",
    );
    assert!(flash_success(resp.cookies()));
    assert_eq!(get_networth(&client), 500);
    for _ in 0..10 {
        let resp = post(
            &client,
            "/transactions",
            "account=test&notes=test&envelope=test&value=500&income=Income&date=2019-01-01",
        );
        assert!(flash_success(resp.cookies()));
    }
    assert_eq!(get_networth(&client), 5500);

    let resp = post(
        &client,
        "/transactions",
        "account=test&notes=test&envelope=test&value=500&income=Expense&date=2019-01-01",
    );
    assert!(flash_success(resp.cookies()));
    assert_eq!(get_networth(&client), 5000);
    // Attempt delete of envelope and account
    let req = client.delete("/api/accounts/test");
    let resp = req.dispatch();
    assert_eq!(flash_success(resp.cookies()), false);
    let req = client.delete("/api/envelope/test");
    let resp = req.dispatch();
    assert_eq!(flash_success(resp.cookies()), false);
}

#[test]
fn transfer() {
    let r = get_rocket();
    let client = Client::new(r).unwrap();
    create_new_user(&client);
    new_account(&client, "test");
    new_account(&client, "test2");
    new_envelope(&client, "test");

    let resp = post(
        &client,
        "/transfer",
        "to_account=test&from_account=test2&to_envelope=test&from_envelope=Uncategorized&\
         value=1982374",
    );
    assert!(flash_success(resp.cookies()));
    assert_eq!(get_networth(&client), 0);
    let resp = post(
        &client,
        "/transfer",
        "to_account=test&from_account=test&to_envelope=test&from_envelope=test&value=1982374",
    );
    assert!(flash_success(resp.cookies()));
    assert_eq!(get_networth(&client), 0);
    let resp = post(
        &client,
        "/transfer",
        "to_account=test&from_account=test&to_envelope=test&from_envelope=Uncategorized&\
         value=1982374",
    );
    assert!(flash_success(resp.cookies()));
    assert_eq!(get_networth(&client), 0);
    let resp = post(
        &client,
        "/transfer",
        "to_account=test&from_account=test2&to_envelope=test&from_envelope=test&value=1982374",
    );
    assert!(flash_success(resp.cookies()));
    assert_eq!(get_networth(&client), 0);
}

#[test]
fn user_crud() {
    let r = get_rocket();
    let client = Client::new(r).unwrap();
    create_new_user(&client);
}

fn get_networth(client: &Client) -> i64 {
    let req = client.get("/networth");
    let mut resp = req.dispatch();
    dbg!(&resp);
    resp.body_string().unwrap().parse().unwrap()
}

fn resp_parse<T: DeserializeOwned>(mut resp: rocket::local::LocalResponse) -> T {
    serde_json::from_str(&resp.body_string().unwrap()).unwrap()
}

fn post<'a>(
    client: &'a Client,
    location: &'static str,
    body: &str,
) -> rocket::local::LocalResponse<'a> {
    let req = client.post(location).body(body).header(ContentType::Form);
    req.dispatch()
}

fn flash_success(v: Vec<Cookie>) -> bool {
    for c in v {
        if c.name() == "_flash" && c.value().contains("success") {
            return true;
        }
    }
    false
}

fn new_account<'a>(client: &'a Client, name: &'static str) -> rocket::local::LocalResponse<'a> {
    let req = client
        .post("/accounts")
        .body(&format!("name={}&currency=USD&priority=0", name))
        .header(ContentType::Form);
    req.dispatch()
}

fn new_envelope<'a>(client: &'a Client, name: &'static str) -> rocket::local::LocalResponse<'a> {
    let req = client
        .post("/envelopes")
        .body(&format!("name={}&currency=USD&category=test", name))
        .header(ContentType::Form);
    req.dispatch()
}

fn create_new_user(client: &Client) {
    let email = format!(
        "{}@test.com",
        uuid::Uuid::new_v4().to_string().split('-').next().unwrap()
    );
    let req = client
        .post("/register")
        .body(&format!(
            "name=foo&password=FooBarPassword&email={}&currency=USD&atleast_13=on",
            email
        ))
        .header(ContentType::Form);
    let response = req.dispatch();
    assert_eq!(response.headers().get("location").next(), Some("/login"));
    let req = client.get(format!("/confirm/{}", email));
    let response = req.dispatch();
    assert!(flash_success(response.cookies()));
    let req = client
        .post("/login")
        .body(&format!("email={}&password=FooBarPassword", email))
        .header(ContentType::Form);
    let response = req.dispatch();
    assert_eq!(response.headers().get("location").next(), Some("/"));
}
