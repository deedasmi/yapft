pub mod accounts;
pub mod categories;
pub mod currency;
pub mod envelopes;

pub use accounts::Account;
use chrono::NaiveDateTime;
pub use envelopes::Envelope;
use std::{error::Error, fmt};
use uuid::Uuid;
pub type Currency = currency::Currency;
pub type MoneyResult = Result<(), MoneyError>;

#[derive(Debug)]
pub enum MoneyError {
    DifferentCurrency,
    InvalidCurrency,
    Overflow,
}

impl fmt::Display for MoneyError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            _ => f.write_str("Error Messages Not Implemented"),
        }
    }
}

impl Error for MoneyError {}

pub trait MoneyOperations {
    fn transfer(&mut self, to: &mut Self, value: i64) -> MoneyResult {
        if self.id() != to.id() {
            if self.currency() == to.currency() {
                self.add(-value)?;
                to.add(value)?;
            } else {
                return Err(MoneyError::DifferentCurrency);
            }
        }
        Ok(())
    }
    fn currency(&self) -> Currency;
    fn add(&mut self, value: i64) -> MoneyResult {
        self.set_value(
            self.value()
                .checked_add(value)
                .ok_or(MoneyError::Overflow)?,
        );
        Ok(())
    }
    fn set_value(&mut self, new: i64);
    fn value(&self) -> i64;
    fn id(&self) -> Uuid;
    fn display_short(&self) -> String {
        self.currency().display_short(self.value())
    }
    fn display_symbol(&self) -> String {
        self.currency().display_symbol(self.value())
    }
}

#[cfg(test)]
mod test {}
