pub mod accounts;
pub mod categories;
pub mod envelopes;
pub mod users;

use crate::{
    auth,
    forms::*,
    models::{
        self, accounts::Account, categories::Category, envelopes::Envelope,
        transactions::PrettyTransaction,
    },
    money::MoneyOperations,
    schema,
    templates::*,
    PgConn,
};
use diesel::prelude::*;
use rocket::{
    http::Status,
    outcome::IntoOutcome,
    request::{FlashMessage, Form, FromRequest, Outcome, Request},
    response::{Flash, Redirect, Responder, Response},
};
use rocket_contrib::json::Json;
use std::{iter::Zip, slice, str::FromStr, vec};
use uuid::Uuid;

type RouteResult = Result<RouteSuccess, RouteFailure>;

pub fn get_routes() -> Vec<rocket::Route> {
    let mut r: Vec<rocket::Route> = Vec::new();
    r.extend(routes![
        home,
        index,
        transactions,
        new_transaction,
        transfer,
        new_transfer,
        networth,
    ]);
    r.extend(self::users::get_routes());
    r.extend(self::accounts::get_routes());
    r.extend(self::envelopes::get_routes());
    r.extend(self::categories::get_routes());
    r
}

#[derive(Debug)]
pub enum RouteSuccess {
    LoginSuccess,
    RegisterSuccess,
    ConfirmSuccess,
    AccountCreated,
    EnvelopeCreated,
    DeleteSuccessful(String),
    TransactionSuccess,
    TransferSuccess,
}

impl RouteSuccess {
    fn location(&self) -> &'static str {
        use RouteSuccess::*;
        match self {
            LoginSuccess => "/",
            RegisterSuccess => "/login",
            ConfirmSuccess => "/login",
            AccountCreated => "/accounts",
            EnvelopeCreated => "/envelopes",
            DeleteSuccessful(_) => "/",
            TransactionSuccess => "/transactions",
            TransferSuccess => "/transfer",
        }
    }

    ///TODO project fluent?
    fn message(&self) -> &'static str {
        use RouteSuccess::*;
        match self {
            LoginSuccess => "Welcome Success",
            RegisterSuccess => "Register Success. Confirm your account.",
            ConfirmSuccess => "Confirmed. Login.",
            _ => "Success",
        }
    }
}

#[derive(Debug)]
pub enum RouteFailure {
    FailedLogin,
    AccountNotConfirmed,
    PasswordError(auth::AuthFailure),
    RegisterFail(String),
    ConfirmFail(String),
    AccountFailed(String),
    EnvelopeFailed(String),
    TransactionFailure(String),
    TransferFailed(String),
}

impl RouteFailure {
    pub fn location(&self) -> &'static str {
        use RouteFailure::*;
        match self {
            FailedLogin => "/login",
            AccountNotConfirmed => "/login",
            TransactionFailure(_) => "/transactions",
            PasswordError(_) => "/login",
            RegisterFail(_) => "/register",
            ConfirmFail(_) => "/login",
            AccountFailed(_) => "/accounts",
            EnvelopeFailed(_) => "/envelopes",
            TransferFailed(_) => "/",
        }
    }

    pub fn message(&self) -> String {
        use RouteFailure::*;
        match self {
            PasswordError(x) => x.to_string(),
            RegisterFail(x) => x.clone(),
            ConfirmFail(x) => x.clone(),
            AccountFailed(x) => x.clone(),
            EnvelopeFailed(x) => x.clone(),
            TransferFailed(x) => x.clone(),
            _ => "Failed".to_string(),
        }
    }
}

impl<'r> Responder<'r> for RouteSuccess {
    fn respond_to(self, req: &Request) -> Result<Response<'r>, Status> {
        Flash::success(Redirect::to(self.location()), self.message()).respond_to(req)
    }
}

impl<'r> Responder<'r> for RouteFailure {
    fn respond_to(self, req: &Request) -> Result<Response<'r>, Status> {
        Flash::error(Redirect::to(self.location()), self.message()).respond_to(req)
    }
}

#[derive(Debug, Clone, Copy)]
pub struct UserSession {
    pub id: Uuid,
}

impl FromStr for UserSession {
    type Err = ();
    fn from_str(cookie: &str) -> Result<Self, Self::Err> {
        let id: Uuid = cookie
            .split('=')
            .last()
            .ok_or(())?
            .parse()
            .map_err(|_| ())?;
        Ok(Self { id })
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for UserSession {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> Outcome<Self, Self::Error> {
        request
            .cookies()
            .get_private("session")
            .and_then(|cookie| cookie.value().parse().ok())
            .or_forward(())
    }
}

///TODO Delete cookie if it's invalid.
#[get("/")]
pub fn home<'a, 'b>(
    conn: PgConn,
    u: UserSession,
    flash: Option<FlashMessage<'a, 'b>>,
) -> Result<HomeTemplate<'a, 'b>, Redirect> {
    use crate::models::users::User;
    use schema::users::dsl::*;
    let user: User = users
        .find(u.id)
        .get_result(&*conn)
        .map_err(|_| Redirect::to("/"))?;
    Ok(HomeTemplate { flash, user })
}

#[get("/", rank = 2)]
pub fn index() -> IndexTemplate {
    IndexTemplate {}
}

#[get("/transactions")]
pub fn transactions<'a, 'b>(
    conn: PgConn,
    user: UserSession,
    flash: Option<FlashMessage<'a, 'b>>,
) -> TransactionTemplate<'a, 'b> {
    let accounts: Vec<Account> = Account::sorted(&user.id)
        .get_results(&*conn)
        .expect("Couldn't get account");
    let envelopes = CategorizedEnvelopes::new(&*conn, &user.id);
    let transactions = PrettyTransaction::new(conn, &user.id, 10);
    TransactionTemplate {
        flash,
        accounts,
        envelopes,
        transactions,
    }
}

///TODO Better return?
#[post("/transactions", data = "<new_transaction>")]
pub fn new_transaction(
    conn: PgConn,
    user: UserSession,
    new_transaction: Form<NewTransactionForm>,
) -> RouteResult {
    use models::transactions::*;
    use schema::{accounts, envelopes, transactions};
    let nt = NewTransaction::new(user.id, new_transaction.into_inner())
        .map_err(|_| RouteFailure::TransactionFailure(String::from("Failed to Parse Date")))?;
    // Start transaction
    conn.transaction(|| {
        // Get Accounts
        dbg!(&nt);
        let mut a: Account = accounts::table.find(nt.account_id).first(&*conn)?;
        let mut e: Envelope = envelopes::table.find(nt.envelope_id).first(&*conn)?;
        //TODO remove unwraps
        // Update values
        a.add(nt.value).unwrap();
        e.add(nt.value).unwrap();
        a.save_changes::<Account>(&*conn)?;
        e.save_changes::<Envelope>(&*conn)?;
        // Insert Transaction
        diesel::insert_into(transactions::table)
            .values(&nt)
            .get_result::<Transaction>(&*conn)
    })
    .map(|_| RouteSuccess::TransactionSuccess)
    .map_err(|e| RouteFailure::TransactionFailure(e.to_string()))
}

#[get("/transfer")]
pub fn transfer<'a, 'b>(conn: PgConn, u: UserSession) -> TransferTemplate<'a, 'b> {
    let accounts: Vec<Account> = Account::by_user_id(&u.id)
        .get_results(&*conn)
        .expect("Couldn't get account");
    let envelopes = CategorizedEnvelopes::new(&*conn, &u.id);
    TransferTemplate {
        flash: None,
        accounts,
        envelopes,
    }
}

#[post("/transfer", data = "<transfer_form>")]
pub fn new_transfer(
    conn: PgConn,
    u: UserSession,
    transfer_form: Form<NewTransferForm>,
) -> RouteResult {
    use schema::{accounts::dsl::accounts, envelopes::dsl::envelopes};
    let tf = transfer_form.into_inner();
    let to_account_id = Uuid::new_v5(&u.id, tf.to_account.as_bytes());
    let from_account_id = Uuid::new_v5(&u.id, tf.from_account.as_bytes());
    let to_envelope_id = Uuid::new_v5(&u.id, tf.to_envelope.as_bytes());
    let from_envelope_id = Uuid::new_v5(&u.id, tf.from_envelope.as_bytes());
    conn.transaction(|| {
        let mut to_account: Account = accounts.find(to_account_id).get_result(&*conn)?;
        let mut from_account: Account = accounts.find(from_account_id).get_result(&*conn)?;
        //TODO Remove unwrap
        from_account.transfer(&mut to_account, tf.value).unwrap();
        to_account.save_changes::<Account>(&*conn)?;
        from_account.save_changes::<Account>(&*conn)?;
        //Envelopes
        let mut to_envelope: Envelope = envelopes.find(to_envelope_id).get_result(&*conn)?;
        let mut from_envelope: Envelope = envelopes.find(from_envelope_id).get_result(&*conn)?;
        //TODO Remove unwrap
        from_envelope.transfer(&mut to_envelope, tf.value).unwrap();
        to_envelope.save_changes::<Envelope>(&*conn)?;
        from_envelope.save_changes::<Envelope>(&*conn)
    })
    .map(|_| RouteSuccess::TransferSuccess)
    .map_err(|e| RouteFailure::TransferFailed(e.to_string()))
}

#[get("/networth")]
pub fn networth(conn: PgConn, u: UserSession) -> Json<i64> {
    let (a, e) = get_accounts_envelopes(conn, &u.id);
    let a_sum: i64 = a.into_iter().map(|x| x.value()).sum();
    let e_sum: i64 = e.into_iter().map(|x| x.value()).sum();
    assert_eq!(a_sum, e_sum);
    Json(a_sum)
}

/// Returns Accounts and Envelopes that belong to the given user
fn get_accounts_envelopes(conn: PgConn, uid: &Uuid) -> (Vec<Account>, Vec<Envelope>) {
    let a: Vec<Account> = Account::sorted(&uid).get_results(&*conn).unwrap();
    let e: Vec<Envelope> = Envelope::sorted(&uid).get_results(&*conn).unwrap();
    (a, e)
}

#[derive(Debug)]
pub struct CategorizedEnvelopes {
    categories: Vec<Category>,
    envelopes:  Vec<Vec<Envelope>>,
}

impl CategorizedEnvelopes {
    /// Error handling
    pub fn new(conn: &PgConnection, uid: &Uuid) -> Self {
        let categories: Vec<Category> = Category::by_user_id(uid)
            .get_results(conn)
            .expect("Couldn't get categories");
        let envelopes = Envelope::categorized(conn, uid, &categories);
        Self {
            categories,
            envelopes,
        }
    }
}

impl<'a> IntoIterator for &'a CategorizedEnvelopes {
    type Item = (&'a Category, &'a Vec<Envelope>);
    type IntoIter = CategorizedIter<'a>;

    fn into_iter(self) -> Self::IntoIter {
        CategorizedIter(self.categories.iter().zip(&self.envelopes))
    }
}

pub struct CategorizedIter<'a>(Zip<slice::Iter<'a, Category>, slice::Iter<'a, Vec<Envelope>>>);

impl<'a> Iterator for CategorizedIter<'a> {
    type Item = (&'a Category, &'a Vec<Envelope>);

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }
}
