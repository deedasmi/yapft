use super::{PgConn, RouteSuccess, Status, UserSession};
use crate::{routes, schema::categories};
use diesel::prelude::*;
use uuid::Uuid;

pub fn get_routes() -> Vec<rocket::Route> {
    routes![delete_categories]
}

#[delete("/category/<name>")]
pub fn delete_categories(
    conn: PgConn,
    user: UserSession,
    name: String,
) -> Result<RouteSuccess, Status> {
    let id = Uuid::new_v5(&user.id, &name.as_bytes());
    // Don't care if delete fails
    let _ = diesel::delete(categories::table.find(id))
        .execute(&*conn)
        .optional();
    Ok(RouteSuccess::DeleteSuccessful(
        uri!(routes::envelopes::envelopes).to_string(),
    ))
}
