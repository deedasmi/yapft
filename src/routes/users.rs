use super::{RouteFailure, RouteResult, RouteSuccess};
use crate::{
    auth,
    forms::{LoginForm, RegisterForm},
    models::users::{NewUser, User},
    schema::users::dsl,
    templates::{LoginTemplate, RegisterTemplate},
    PgConn,
};
use diesel::prelude::*;
use rocket::{
    http::{Cookie, Cookies},
    request::{FlashMessage, Form},
    response::Redirect,
};

pub fn get_routes() -> Vec<rocket::Route> {
    routes![login, post_login, register, post_register, confirm]
}

#[get("/login")]
pub fn login<'a, 'b>(
    flash: Option<FlashMessage<'a, 'b>>,
    mut cookies: Cookies,
) -> Result<Redirect, LoginTemplate<'a, 'b>> {
    cookies
        .get_private("session")
        .map(|_s| Redirect::to("/"))
        .ok_or(LoginTemplate { flash })
}

#[post("/login", data = "<login>")]
pub fn post_login(
    conn: PgConn,
    mut cookies: Cookies,
    login: Form<LoginForm>,
) -> Result<Redirect, RouteFailure> {
    use dsl::*;
    let login = login.into_inner();
    let u: User = users
        .filter(email.eq(&*login.email))
        .get_result(&*conn)
        .map_err(|_| RouteFailure::FailedLogin)?;
    u.is_confirmed()
        .map_err(|_| RouteFailure::AccountNotConfirmed)?;
    use auth::AuthResult::*;
    match auth::check(login.password, u.get_phc()) {
        Success => Ok(login_success(u, &mut cookies)),
        Update(n) => {
            //TODO#10 update password
            if let Err(e) = diesel::update(users.find(u.id))
                .set(phc.eq(n))
                .get_result::<User>(&*conn)
            {
                warn!("User failed to update password! Error: {}", e.to_string());
            }
            Ok(login_success(u, &mut cookies))
        },
        Failed(_) => Err(RouteFailure::FailedLogin),
    }
}

fn login_success(u: User, cookies: &mut Cookies) -> Redirect {
    //TODO Figure out how to make cookie expire
    let c = Cookie::new("session", format!("id={}", u.id,));
    cookies.add_private(c);
    Redirect::to("/")
}

#[get("/register")]
pub fn register<'a, 'b>(flash: Option<FlashMessage<'a, 'b>>) -> RegisterTemplate<'a, 'b> {
    RegisterTemplate { flash }
}

#[post("/register", data = "<register>")]
pub fn post_register(conn: PgConn, register: Form<RegisterForm>) -> RouteResult {
    let user: NewUser = NewUser::from(register.into_inner());
    user.insert(&*conn)
        .map(|_| RouteSuccess::RegisterSuccess)
        .map_err(|e| RouteFailure::RegisterFail(e.to_string()))
}

///TODO#12 replace this
#[get("/confirm/<uemail>")]
pub fn confirm(conn: PgConn, uemail: String) -> RouteResult {
    use crate::{forms::NewEnvelopeForm, models::categories::Category, schema::categories};
    use dsl::*;

    conn.transaction(|| {
        // Get user
        let u: User = users
            .filter(email.eq(uemail))
            .first::<User>(&*conn)?
            .confirm()
            .save_changes(&*conn)?;

        //Create base envelope
        use crate::{money::envelopes::NewEnvelope, schema::envelopes};
        let nef = NewEnvelopeForm {
            name: String::from("Uncategorized"),
            currency: u.primary_currency,
            category: String::from("Uncategorized"),
        };
        let c = Category::new(u.id, String::from("Uncategorized"));
        diesel::insert_into(categories::table)
            .values(&c)
            .execute(&*conn)?;
        let e = NewEnvelope::new(&u.id, &nef);
        diesel::insert_into(envelopes::table)
            .values(&e)
            .execute(&*conn)
    })
    .map(|_| RouteSuccess::ConfirmSuccess)
    .map_err(|e| RouteFailure::ConfirmFail(e.to_string()))
}
