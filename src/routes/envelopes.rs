use super::{CategorizedEnvelopes, RouteFailure, RouteResult, RouteSuccess, UserSession};
use crate::{
    forms::NewEnvelopeForm,
    models::{
        categories::Category,
        envelopes::{Envelope, NewEnvelope, PrettyEnvelope},
    },
    money::MoneyOperations,
    schema::envelopes::dsl,
    templates::EnvelopesTemplate,
    PgConn,
};
use diesel::prelude::*;
use rocket::{
    http::Status,
    request::{FlashMessage, Form},
};
use rocket_contrib::json::Json;
use uuid::Uuid;

pub fn get_routes() -> Vec<rocket::Route> {
    routes![
        envelopes,
        new_envelope,
        api_delete_envelope,
        api_get_envelope,
        api_get_envelopes
    ]
}

#[get("/envelopes")]
pub fn envelopes<'a, 'b>(
    conn: PgConn,
    user: UserSession,
    flash: Option<FlashMessage<'a, 'b>>,
) -> EnvelopesTemplate<'a, 'b> {
    let envelopes = CategorizedEnvelopes::new(&*conn, &user.id);
    //TODO error handle
    EnvelopesTemplate { flash, envelopes }
}

#[get("/api/envelopes")]
pub fn api_get_envelopes(
    conn: PgConn,
    user: UserSession,
) -> Result<Json<Vec<PrettyEnvelope>>, Status> {
    Ok(Json(PrettyEnvelope::by_user_id(conn, &user.id)))
}

#[get("/api/envelopes/<name>")]
pub fn api_get_envelope(
    conn: PgConn,
    user: UserSession,
    name: String,
) -> Result<Json<PrettyEnvelope>, Status> {
    let id = Uuid::new_v5(&user.id, name.as_bytes());
    Ok(Json(PrettyEnvelope::by_id(conn, &id)))
}

#[post("/envelopes", data = "<new>")]
pub fn new_envelope(conn: PgConn, user: UserSession, new: Form<NewEnvelopeForm>) -> RouteResult {
    use crate::schema::categories;
    use dsl::*;
    let f = new.into_inner();
    let na = NewEnvelope::new(&user.id, &f);
    conn.transaction(|| {
        diesel::insert_into(categories::table)
            .values(&Category::new(user.id, f.category.clone()))
            .on_conflict(categories::id)
            .do_nothing()
            .execute(&*conn)?;
        diesel::insert_into(envelopes)
            .values(&na)
            .on_conflict(id)
            .do_update()
            .set(category_id.eq(na.category_id))
            .get_result::<Envelope>(&*conn)
    })
    .map(|_| RouteSuccess::EnvelopeCreated)
    .map_err(|e| RouteFailure::EnvelopeFailed(e.to_string()))
}

#[delete("/api/envelopes/<name>")]
pub fn api_delete_envelope(
    conn: PgConn,
    user: UserSession,
    name: String,
) -> Result<RouteSuccess, Status> {
    use dsl::envelopes;
    let id = Uuid::new_v5(&user.id, &name.as_bytes());
    let e: Envelope = envelopes
        .find(id)
        .first(&*conn)
        .map_err(|_| Status::InternalServerError)?;
    if e.name == "Uncategorized" {
        return Err(Status::Forbidden);
    }
    if e.value() != 0 {
        return Err(Status::Conflict);
    }
    // Don't care if delete fails
    let _ = diesel::delete(envelopes.find(id))
        .execute(&*conn)
        .optional();
    Ok(RouteSuccess::DeleteSuccessful(uri!(envelopes).to_string()))
}
