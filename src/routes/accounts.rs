use super::{RouteFailure, RouteResult, RouteSuccess, UserSession};
use crate::{
    forms::NewAccountForm,
    models::accounts::{Account, NewAccount},
    money::MoneyOperations,
    schema::accounts::dsl,
    templates::AccountsTemplate,
    PgConn,
};
use diesel::prelude::*;
use rocket::{
    http::Status,
    request::{FlashMessage, Form},
};
use rocket_contrib::json::Json;
use uuid::Uuid;

pub fn get_routes() -> Vec<rocket::Route> {
    routes![
        api_get_accounts,
        api_get_account,
        accounts,
        new_account,
        api_delete_account
    ]
}

#[get("/accounts")]
pub fn accounts<'a, 'b>(
    conn: PgConn,
    user: UserSession,
    flash: Option<FlashMessage<'a, 'b>>,
) -> AccountsTemplate<'a, 'b> {
    let a: Vec<Account> = Account::sorted(&user.id).get_results(&*conn).unwrap();
    //TODO error handle
    AccountsTemplate { flash, accounts: a }
}

#[get("/api/accounts")]
pub fn api_get_accounts(conn: PgConn, user: UserSession) -> Result<Json<Vec<Account>>, Status> {
    //TODO match 404
    Account::by_user_id(&user.id)
        .get_results(&*conn)
        .map(Json)
        .map_err(|_| Status::InternalServerError)
}

#[get("/api/accounts/<name>")]
pub fn api_get_account(
    conn: PgConn,
    user: UserSession,
    name: String,
) -> Result<Json<Account>, Status> {
    use dsl::accounts;
    let aid = Uuid::new_v5(&user.id, name.as_bytes());
    //TODO match 404
    accounts
        .find(aid)
        .first(&*conn)
        .map(Json)
        .map_err(|_| Status::InternalServerError)
}

#[post("/accounts", data = "<new>")]
pub fn new_account(conn: PgConn, user: UserSession, new: Form<NewAccountForm>) -> RouteResult {
    use dsl::*;
    let f = new.into_inner();
    let na = NewAccount::new(&user.id, &f);
    diesel::insert_into(accounts)
        .values(&na)
        .on_conflict(id)
        .do_update()
        .set(priority.eq(na.priority))
        .get_result::<Account>(&*conn)
        .map(|_| RouteSuccess::AccountCreated)
        .map_err(|e| RouteFailure::AccountFailed(e.to_string()))
}

#[delete("/api/accounts/<name>")]
pub fn api_delete_account(
    conn: PgConn,
    user: UserSession,
    name: String,
) -> Result<RouteSuccess, Status> {
    use dsl::accounts;
    let id = Uuid::new_v5(&user.id, name.as_bytes());
    let a: Account = accounts
        .find(id)
        .first(&*conn)
        .map_err(|_| Status::InternalServerError)?;
    if a.value() != 0 {
        return Err(Status::Conflict);
    }
    // Don't care if delete fails
    let _ = diesel::delete(accounts.find(id)).execute(&*conn).optional();
    Ok(RouteSuccess::DeleteSuccessful(uri!(accounts).to_string()))
}
