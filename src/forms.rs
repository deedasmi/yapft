use rocket::{http::RawStr, request::FromFormValue};
use std::{fmt, ops::Deref};

#[derive(FromForm, Debug)]
pub struct RegisterForm {
    pub name: String,
    pub password: Password,
    pub email: Email,
    pub currency: crate::money::Currency,
    pub atleast_13: bool,
}

#[derive(FromForm, Debug)]
pub struct LoginForm {
    pub email:    Email,
    pub password: Password,
}

#[derive(FromForm, Debug)]
pub struct NewAccountForm {
    pub name: String,
    pub currency: crate::money::Currency,
    pub priority: i16,
}

#[derive(FromForm, Debug)]
pub struct NewEnvelopeForm {
    pub name: String,
    pub currency: crate::money::Currency,
    pub category: String,
}

#[derive(FromForm, Debug)]
pub struct NameForm {
    pub name: String,
}

#[derive(FromForm, Debug)]
pub struct NewTransactionForm {
    pub account: String,
    pub notes: String,
    pub envelope: String,
    pub value: i64,
    pub income: IncomeExpense,
    pub date: String,
}

#[derive(FromForm, Debug)]
pub struct NewTransferForm {
    pub from_account: String,
    pub to_account: String,
    pub from_envelope: String,
    pub to_envelope: String,
    pub value: i64,
}

#[derive(PartialEq, FromFormValue, Debug)]
pub enum IncomeExpense {
    Income,
    Expense,
}

pub struct Password(String);

impl Password {
    pub fn new(x: String) -> Self {
        Password(x)
    }
    pub fn get(self) -> String {
        self.0
    }
}

/// Password should not leak value while debugging
/// # Example
/// ```
/// let p = Password(String::From("Foobar");
/// assert_ne!(format!("{:?}").contains('F'), false);
/// ```
impl fmt::Debug for Password {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("**SECRET**")
    }
}

impl<'v> FromFormValue<'v> for Password {
    type Error = &'v RawStr;

    fn from_form_value(form_value: &'v RawStr) -> Result<Password, &'v RawStr> {
        if form_value.len() < 8 || form_value.len() > 255 {
            Err(form_value)
        } else {
            form_value
                .url_decode()
                .map(Password)
                .map_err(|_| RawStr::from_str("Invalid UTF8"))
        }
    }
}

#[derive(Debug)]
pub struct Email(String);

impl Email {
    pub fn new(s: String) -> Email {
        Email(s)
    }

    pub fn inner(self) -> String {
        self.0
    }
}

impl<'v> FromFormValue<'v> for Email {
    type Error = ();

    fn from_form_value(form_value: &'v RawStr) -> Result<Self, Self::Error> {
        use fast_chemail::is_valid_email;
        if is_valid_email(&form_value.url_decode().map_err(|_| ())?) {
            form_value.url_decode().map(Email).map_err(|_| ())
        } else {
            Err(())
        }
    }
}

impl Deref for Email {
    type Target = str;
    fn deref(&self) -> &str {
        &self.0
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn no_leak_password() {
        let p = Password(String::from("Foobar"));
        assert_eq!(format!("{:?}", p).contains('F'), false)
    }
}
