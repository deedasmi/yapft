use crate::{
    forms::{IncomeExpense, NewTransactionForm},
    models::{accounts::Account, envelopes::Envelope},
    schema::{
        accounts::table as accounts,
        envelopes::table as envelopes,
        transactions::{self, dsl},
    },
    PgConn,
};
use chrono::NaiveDate;
use diesel::{
    helper_types::{Desc, Eq, Filter, InnerJoin, Limit, Order},
    prelude::*,
};
use uuid::Uuid;

type IDFilter<'a> = Filter<transactions::table, Eq<dsl::user_id, &'a Uuid>>;
type Latest<'a> = Limit<Order<IDFilter<'a>, Desc<dsl::tdate>>>;

#[derive(Debug, Queryable, Identifiable, PartialEq)]
pub struct Transaction {
    id: Uuid,
    user_id: Uuid,
    pub account_id: Uuid,
    pub envelope_id: Uuid,
    pub notes: String,
    pub value: i64,
    pub tdate: NaiveDate,
}

impl Transaction {
    pub fn by_user_id(uid: &Uuid) -> IDFilter {
        dsl::transactions.filter(dsl::user_id.eq(uid))
    }
    pub fn latest(uid: &Uuid, limit: i64) -> Latest {
        Self::by_user_id(uid)
            .order_by(dsl::tdate.desc())
            .limit(limit)
    }
    pub fn complete(uid: &Uuid, limit: i64) -> InnerJoin<InnerJoin<Latest, accounts>, envelopes> {
        Self::latest(uid, limit)
            .inner_join(accounts)
            .inner_join(envelopes)
    }
}

impl NewTransaction {
    pub fn new(user_id: Uuid, ntf: NewTransactionForm) -> Result<Self, ()> {
        let id = Uuid::new_v4();
        dbg!(&ntf);
        let account_id: Uuid = Uuid::new_v5(&user_id, &ntf.account.as_bytes());
        let envelope_id: Uuid = Uuid::new_v5(&user_id, &ntf.envelope.as_bytes());
        let v = ntf.value.abs();
        let value = if ntf.income == IncomeExpense::Income {
            v
        } else {
            -v
        };
        Ok(NewTransaction {
            id,
            user_id,
            account_id,
            envelope_id,
            notes: ntf.notes,
            value,
            tdate: ntf.date.parse().map_err(|_| ())?,
        })
    }
}

#[derive(Debug, Identifiable, Insertable)]
#[table_name = "transactions"]
pub struct NewTransaction {
    id: Uuid,
    pub user_id: Uuid,
    pub account_id: Uuid,
    pub envelope_id: Uuid,
    pub notes: String,
    pub value: i64,
    pub tdate: NaiveDate,
}

#[derive(Debug)]
pub struct PrettyTransaction {
    pub account:  String,
    pub envelope: String,
    pub notes:    String,
    pub value:    String,
    pub tdate:    NaiveDate,
}

impl PrettyTransaction {
    /// TODO Error Handling
    pub fn new(conn: PgConn, uid: &Uuid, limit: i64) -> Vec<PrettyTransaction> {
        let all: Vec<(Transaction, Account, Envelope)> = Transaction::complete(uid, limit)
            .get_results(&*conn)
            .unwrap();
        let mut r: Vec<PrettyTransaction> = Vec::with_capacity(limit as usize);
        for (t, a, e) in all {
            let n = PrettyTransaction {
                account:  a.name,
                envelope: e.name,
                notes:    t.notes,
                value:    a.currency.display_symbol(t.value),
                tdate:    t.tdate,
            };
            r.push(n);
        }
        r
    }
}
