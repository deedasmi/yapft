use crate::{auth, money::Currency, schema::users};
use chrono::NaiveDateTime;
use diesel::{dsl::Eq, pg::PgConnection, prelude::*};
use uuid::Uuid;

pub type WithEmail<'a> = Eq<users::email, &'a str>;

pub fn with_name(email: &str) -> WithEmail {
    users::email.eq(email)
}

#[derive(Debug, Queryable, AsChangeset, Identifiable, PartialEq)]
#[table_name = "users"]
pub struct User {
    pub id: Uuid,
    pub name: String,
    pub email: String,
    phc: String,
    confirmed: bool,
    pub user_type: i16,
    pub primary_currency: Currency,
    joined: NaiveDateTime,
    last_login: Option<NaiveDateTime>,
}

impl User {
    pub fn get_phc(&self) -> &str {
        &self.phc
    }
    pub fn is_confirmed(&self) -> Result<(), ()> {
        if self.confirmed {
            Ok(())
        } else {
            Err(())
        }
    }
    pub fn confirm(mut self) -> Self {
        self.confirmed = true;
        self
    }
}

#[derive(Debug, Insertable)]
#[table_name = "users"]
pub struct NewUser {
    id: Uuid,
    name: String,
    phc: String,
    email: String,
    primary_currency: Currency,
}

impl NewUser {
    pub fn new(name: String, phc: String, email: String, primary_currency: Currency) -> Self {
        NewUser {
            id: Uuid::new_v4(),
            name,
            phc,
            email,
            primary_currency,
        }
    }
    pub fn insert(&self, conn: &PgConnection) -> Result<User, String> {
        diesel::insert_into(users::table)
            .values(self)
            .get_result(conn)
            .map_err(|e| e.to_string())
    }
}

impl From<crate::forms::RegisterForm> for NewUser {
    fn from(r: crate::forms::RegisterForm) -> Self {
        Self::new(
            r.name,
            auth::hash(r.password).unwrap(),
            r.email.inner(),
            r.currency,
        )
    }
}
