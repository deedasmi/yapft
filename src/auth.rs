#![allow(unreachable_code)]
use crate::forms::Password;
use libreauth::pass;
use std::fmt;

const PWD_SCHEME_VERSION: usize = 1;

#[derive(Debug, PartialEq)]
pub enum AuthResult {
    Success,
    Update(String),
    Failed(AuthFailure),
}

#[derive(Debug, PartialEq)]
pub enum AuthFailure {
    TooShort,
    TooLong,
    Pwned,
    IncorrectPassword,
}

impl From<pass::ErrorCode> for AuthFailure {
    fn from(e: pass::ErrorCode) -> Self {
        use pass::ErrorCode::*;
        use AuthFailure::*;
        match e {
            PasswordTooShort => TooShort,
            PasswordTooLong => TooLong,
            _ => unreachable!("Password failure!"),
        }
    }
}

impl fmt::Display for AuthFailure {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use AuthFailure::*;
        match self {
            TooShort => f.write_str("Password must be 8 characters long"),
            TooLong => f.write_str("Password must be less than 128 characters"),
            Pwned => f.write_str("Password compromised!"),
            IncorrectPassword => f.write_str("Incorrect password"),
        }
    }
}

///TODO Review HashBuilder options
fn build_hasher() -> pass::Hasher {
    pass::HashBuilder::new()
        .version(PWD_SCHEME_VERSION)
        .finalize()
        .unwrap()
}

/// Hashes a given password and returns either a self-contained PHC string or an
/// error.
pub fn hash(password: Password) -> Result<String, AuthFailure> {
    //TODO#9 have they been pwned
    build_hasher()
        .hash(&password.get().as_str())
        .map_err(AuthFailure::from)
}

/// Checks a given password against a given PHC.
pub fn check(password: Password, db_password: &str) -> AuthResult {
    let hasher = pass::HashBuilder::from_phc(db_password).unwrap();
    if hasher.is_valid(&password.get()) {
        //TODO#10 update password
        AuthResult::Success
    } else {
        AuthResult::Failed(AuthFailure::IncorrectPassword)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn good_password() {
        let p: Password = Password::new(String::from("FoobarPass"));
        let p2: Password = Password::new(String::from("FoobarPass"));
        let phc = hash(p);
        assert!(phc.is_ok());
        let phc = phc.unwrap();
        assert_eq!(check(p2, &phc), AuthResult::Success);
    }

    #[test]
    fn short_password() {
        let p: Password = Password::new(String::from("Foobar"));
        let phc = hash(p);
        assert!(phc.is_err());
    }

    #[test]
    fn bad_password() {
        let p: Password = Password::new(String::from("FoobarPass"));
        let p2: Password = Password::new(String::from("FooBarPass"));
        let phc = hash(p);
        assert!(phc.is_ok());
        let phc = phc.unwrap();
        assert_eq!(
            check(p2, &phc),
            AuthResult::Failed(AuthFailure::IncorrectPassword)
        );
    }

    #[test]
    fn pwned_password() {
        //TODO
        return;
        let p: Password = Password::new(String::from("correcthorsebatterystaple"));
        let phc = hash(p);
        assert!(phc.is_err());
    }

    #[test]
    fn update_password() {
        //TODO
        return;
        let _p = Password::new(String::from("FoobarPass"));
        let _phc = String::from(
            "$argon2$passes=3,norm=nfkc,len=128,lanes=4,ver=0,len-calc=chars,pmin=8,mem=12,\
             pmax=128$zIoWkxf2FWRDq6Zj3ELF2g$Gz0t
X0bLOngNPbthEmq3j732f9dvy65f+ITJgOKpoWAPIyLA/0k5qa1IS8ZnU/rAZ1VbJnCdQ2oR9TsdcjCru8FalqOkKu5QTS
P3MV/kUPBdpj0u2YZgOw1CRM1wHFY91B6MhHYfARwpVfDfK6SRKs7aHq/TAuZCW4LB+WvO0xw",
        );
        //TODO figure out how to do this
        //assert_eq!(check(p, &phc), AuthResult::Update);
    }
}
