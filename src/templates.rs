extern crate askama;
use crate::{
    models::{accounts::*, transactions::PrettyTransaction},
    money::MoneyOperations,
    routes::CategorizedEnvelopes,
};
use askama::Template;
use rocket::request::FlashMessage;

#[derive(Template)]
#[template(path = "index.html")]
pub struct IndexTemplate;

#[derive(Template, Debug)]
#[template(path = "home.html")]
pub struct HomeTemplate<'f, 'r> {
    pub flash: Option<FlashMessage<'f, 'r>>,
    pub user:  crate::models::users::User,
}

#[derive(Template, Debug)]
#[template(path = "register.html")]
pub struct RegisterTemplate<'f, 'r> {
    pub flash: Option<FlashMessage<'f, 'r>>,
}

#[derive(Template, Debug)]
#[template(path = "login.html")]
pub struct LoginTemplate<'f, 'r> {
    pub flash: Option<FlashMessage<'f, 'r>>,
}

#[derive(Template, Debug)]
#[template(path = "accounts.html")]
pub struct AccountsTemplate<'f, 'r> {
    pub flash:    Option<FlashMessage<'f, 'r>>,
    pub accounts: Vec<Account>,
}

#[derive(Template, Debug)]
#[template(path = "envelopes.html")]
pub struct EnvelopesTemplate<'f, 'r> {
    pub flash: Option<FlashMessage<'f, 'r>>,
    pub envelopes: CategorizedEnvelopes,
}

#[derive(Template, Debug)]
#[template(path = "transaction.html")]
pub struct TransactionTemplate<'f, 'r> {
    pub flash: Option<FlashMessage<'f, 'r>>,
    pub accounts: Vec<Account>,
    pub envelopes: CategorizedEnvelopes,
    pub transactions: Vec<PrettyTransaction>,
}

#[derive(Template, Debug)]
#[template(path = "transfer.html")]
pub struct TransferTemplate<'f, 'r> {
    pub flash: Option<FlashMessage<'f, 'r>>,
    pub accounts: Vec<Account>,
    pub envelopes: CategorizedEnvelopes,
}
