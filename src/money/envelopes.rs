use super::*;
use crate::{
    forms::NewEnvelopeForm,
    models::categories::Category,
    money::Currency,
    schema::envelopes::{self, dsl},
    PgConn,
};
use diesel::{
    associations::GroupedBy,
    helper_types::{Asc, Eq, Filter, Order},
    prelude::*,
};
use serde_derive::{Deserialize, Serialize};

type IDFilter<'a> = Filter<envelopes::table, Eq<dsl::user_id, &'a Uuid>>;

#[derive(Debug, Clone, Queryable, Associations, AsChangeset, Identifiable, PartialEq)]
#[belongs_to(Category)]
#[table_name = "envelopes"]
pub struct Envelope {
    id: Uuid,
    user_id: Uuid,
    pub name: String,
    created_at: NaiveDateTime,
    value: i64,
    pub currency: Currency,
    pub category_id: Uuid,
}

impl Envelope {
    pub fn by_user_id(uid: &Uuid) -> IDFilter {
        dsl::envelopes.filter(dsl::user_id.eq(uid))
    }
    ///TODO Error handling
    pub fn categorized(
        conn: &PgConnection,
        uid: &Uuid,
        categories: &[Category],
    ) -> Vec<Vec<Envelope>> {
        // Is sorted neccesary now?
        Envelope::sorted(uid)
            .load::<Envelope>(conn)
            .expect("Failed to group categories")
            .grouped_by(&categories)
    }
    pub fn sorted<'a>(u: &'a Uuid) -> Order<IDFilter<'a>, Asc<dsl::category_id>> {
        Envelope::by_user_id(u).order_by(dsl::category_id.asc())
    }
}

#[derive(Debug, Insertable)]
#[table_name = "envelopes"]
pub struct NewEnvelope<'a> {
    id: Uuid,
    user_id: &'a Uuid,
    pub name: &'a str,
    pub currency: Currency,
    pub category_id: Uuid,
}

impl<'a> NewEnvelope<'a> {
    pub fn new(user_id: &'a Uuid, f: &'a NewEnvelopeForm) -> Self {
        let uuid = Uuid::new_v5(user_id, f.name.as_bytes());
        let category_id = Uuid::new_v5(user_id, &f.category.as_bytes());
        NewEnvelope {
            id: uuid,
            user_id,
            name: &f.name,
            currency: f.currency,
            category_id,
        }
    }
}

impl MoneyOperations for Envelope {
    fn currency(&self) -> Currency {
        self.currency
    }
    fn value(&self) -> i64 {
        self.value
    }
    fn set_value(&mut self, new: i64) {
        self.value = new;
    }
    fn id(&self) -> Uuid {
        self.id
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PrettyEnvelope {
    pub name: String,
    pub created_at: NaiveDateTime,
    pub value: i64,
    pub currency: Currency,
    pub category: String,
}

impl PrettyEnvelope {
    pub fn by_user_id(conn: PgConn, uid: &Uuid) -> Vec<PrettyEnvelope> {
        Envelope::by_user_id(&uid)
            .inner_join(crate::schema::categories::table)
            .get_results::<(Envelope, Category)>(&*conn)
            .unwrap()
            .into_iter()
            .map(PrettyEnvelope::from)
            .collect()
    }
    pub fn by_id(conn: PgConn, id: &Uuid) -> PrettyEnvelope {
        use dsl::envelopes;
        envelopes
            .find(id)
            .inner_join(crate::schema::categories::table)
            .first::<(Envelope, Category)>(&*conn)
            .map(PrettyEnvelope::from)
            .unwrap()
    }
}

impl From<(Envelope, Category)> for PrettyEnvelope {
    fn from(ec: (Envelope, Category)) -> Self {
        let (e, c) = ec;
        PrettyEnvelope {
            name: e.name,
            created_at: e.created_at,
            value: e.value,
            currency: e.currency,
            category: c.name,
        }
    }
}
