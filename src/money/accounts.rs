use super::*;
use crate::{
    forms::NewAccountForm,
    money::Currency,
    schema::accounts::{self, dsl},
};
use diesel::{
    helper_types::{Desc, Eq, Filter, Order},
    prelude::*,
};
use serde_derive::{Deserialize, Serialize};

type IDFilter<'a> = Filter<accounts::table, Eq<dsl::user_id, &'a Uuid>>;

#[derive(Debug, Deserialize, Queryable, Serialize, AsChangeset, Identifiable, PartialEq)]
#[table_name = "accounts"]
pub struct Account {
    #[serde(skip)]
    id: Uuid,
    #[serde(skip)]
    user_id: Uuid,
    pub name: String,
    created_at: NaiveDateTime,
    value: i64,
    pub currency: Currency,
    pub priority: i16,
}

impl Account {
    pub fn by_user_id(uid: &Uuid) -> IDFilter {
        dsl::accounts.filter(dsl::user_id.eq(uid))
    }
    pub fn sorted<'a>(uid: &'a Uuid) -> Order<IDFilter<'a>, Desc<dsl::priority>> {
        Self::by_user_id(uid).order_by(dsl::priority.desc())
    }
}

#[derive(Debug, Insertable)]
#[table_name = "accounts"]
pub struct NewAccount<'a> {
    pub id: Uuid,
    user_id: &'a Uuid,
    pub name: &'a str,
    pub currency: Currency,
    pub priority: i16,
}

impl<'a> NewAccount<'a> {
    pub fn new(user_id: &'a Uuid, f: &'a NewAccountForm) -> Self {
        let uuid = Uuid::new_v5(user_id, f.name.as_bytes());
        NewAccount {
            id: uuid,
            user_id,
            name: &f.name,
            currency: f.currency,
            priority: f.priority,
        }
    }
}

impl MoneyOperations for Account {
    fn currency(&self) -> Currency {
        self.currency
    }
    fn value(&self) -> i64 {
        self.value
    }
    fn set_value(&mut self, new: i64) {
        self.value = new;
    }
    fn id(&self) -> Uuid {
        self.id
    }
}
