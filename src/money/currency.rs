use super::MoneyError;
use diesel::{
    deserialize::{self, FromSql},
    pg::Pg,
    serialize::{self, IsNull, Output, ToSql},
};
use rocket::{http::RawStr, request::FromFormValue};
use serde::{
    de::{self, Deserialize, Deserializer},
    ser::{Serialize, Serializer},
};
use std::{io::Write, str::FromStr};

#[derive(Debug, Eq, PartialEq, FromSqlRow, AsExpression, Clone, Copy)]
#[sql_type = "CurrencySQLType"]
pub struct Currency {
    pub name: &'static str,
    pub short_name: &'static str,
    pub symbol: &'static str,
    symbol_front: bool,
    dollar_separator: &'static str,
    cent_separator: Option<&'static str>,
}

impl Serialize for Currency {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(self.short_name)
    }
}

impl<'de> Deserialize<'de> for Currency {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        FromStr::from_str(&s).map_err(de::Error::custom)
    }
}

impl<'v> FromFormValue<'v> for Currency {
    type Error = ();

    fn from_form_value(f: &'v RawStr) -> Result<Self, Self::Error> {
        f.percent_decode()
            .map(|s| Currency::from_str(&s).map_err(|_| ()))
            .map_err(|_| ())?
    }
}

impl FromStr for Currency {
    type Err = super::MoneyError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "$" | "USD" => Ok(USD),
            "¥" | "JPY" => Ok(JPY),
            _ => Err(MoneyError::InvalidCurrency),
        }
    }
}

impl Currency {
    pub fn display_symbol(&self, value: i64) -> String {
        let mut s = String::new();
        if self.symbol_front {
            s.push_str(self.symbol);
        }
        self.format_number(value, &mut s);
        if !self.symbol_front {
            s.insert_str(s.find(')').unwrap_or_else(|| s.len()), self.symbol);
        }
        s
    }

    pub fn display_short(&self, value: i64) -> String {
        let mut s = String::new();
        self.format_number(value, &mut s);
        s.push_str(" ");
        s.push_str(self.short_name);
        s
    }

    pub fn display_both(&self, value: i64) -> String {
        let mut s = String::new();
        if self.symbol_front {
            s.push_str(self.symbol);
        }
        self.format_number(value, &mut s);
        if !self.symbol_front {
            s.insert_str(s.find(')').unwrap_or_else(|| s.len()), self.symbol);
        }
        s.push_str(" ");
        s.push_str(self.short_name);
        s
    }

    /// Adds all the commas and periods and such in a number
    /// GOTCHA: i64::min_value breaks currencies without a cent seperator.
    /// Currently the display adds one, but the value hasn't changed..
    fn format_number(&self, mut value: i64, s: &mut String) {
        let neg = value.is_negative();
        let cents: String = match self.cent_separator {
            Some(x) => {
                let cents = value % 100;
                value = (value - cents) / 100;
                if cents == 0 {
                    format!("{}00", x)
                } else if cents.abs() < 10 {
                    format!("{}0{}", x, cents.abs())
                } else {
                    format!("{}{}", x, cents.abs())
                }
            },
            None => "".to_string(),
        };
        //TODO This better
        if value == i64::min_value() {
            value += 1;
        }
        let v = value.abs().to_string();
        let l = v.len();
        for (i, ch) in v.chars().enumerate() {
            if i != 0 && (l - i) % 3 == 0 {
                s.push_str(self.dollar_separator);
            }
            s.push(ch);
        }
        s.push_str(&cents);
        if neg {
            s.push(')');
            s.insert(0, '(');
        }
    }
}

impl ToSql<CurrencySQLType, Pg> for Currency {
    fn to_sql<W: Write>(&self, out: &mut Output<W, Pg>) -> serialize::Result {
        out.write_all(self.short_name.as_bytes())?;
        Ok(IsNull::No)
    }
}

impl FromSql<CurrencySQLType, Pg> for Currency {
    fn from_sql(bytes: Option<&[u8]>) -> deserialize::Result<Self> {
        match not_none!(bytes) {
            b"USD" => Ok(USD),
            b"JPY" => Ok(JPY),
            _ => Err(Box::new(MoneyError::InvalidCurrency)),
        }
    }
}

pub const USD: Currency = Currency {
    name: "United States Dollar",
    short_name: "USD",
    symbol: "$",
    symbol_front: true,
    dollar_separator: ",",
    cent_separator: Some("."),
};

pub const JPY: Currency = Currency {
    name: "Japanese Yen",
    short_name: "JPY",
    symbol: "¥",
    symbol_front: false,
    dollar_separator: ",",
    cent_separator: None,
};

#[derive(SqlType)]
#[postgres(type_name = "Currency")]
pub struct CurrencySQLType;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn usd() {
        let u = Currency::from_str("USD").unwrap();
        assert_eq!(u.display_short(192837465), "1,928,374.65 USD");
        assert_eq!(u.display_symbol(-0987654321), "($9,876,543.21)");
        assert_eq!(
            u.display_both(i64::max_value()),
            "$92,233,720,368,547,758.07 USD"
        );
        assert_eq!(
            u.display_both(i64::min_value()),
            "($92,233,720,368,547,758.08) USD"
        );
        assert_eq!(u.display_symbol(0), "$0.00");
        assert_eq!(u.display_both(-5), "($0.05) USD");
        assert_eq!(u.display_both(5), "$0.05 USD");
    }

    #[test]
    fn jpy() {
        let j: Currency = JPY;
        assert_eq!(j.display_short(5), "5 JPY");
        assert_eq!(
            j.display_short(i64::min_value()),
            "(9,223,372,036,854,775,807) JPY"
        );
    }
}
