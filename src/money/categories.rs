use crate::schema::categories::{self, dsl};
use diesel::{
    helper_types::{Eq, Filter},
    prelude::*,
};
use uuid::Uuid;

type IDFilter<'a> = Filter<categories::table, Eq<dsl::user_id, &'a Uuid>>;

#[derive(Debug, Insertable, Queryable, AsChangeset, Identifiable, PartialEq)]
#[table_name = "categories"]
pub struct Category {
    id: Uuid,
    pub user_id: Uuid,
    pub name: String,
}

impl std::fmt::Display for Category {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.name)
    }
}

impl Category {
    pub fn by_user_id(uid: &Uuid) -> IDFilter {
        dsl::categories.filter(dsl::user_id.eq(uid))
    }
    pub fn new(user_id: Uuid, name: String) -> Self {
        Category {
            id: Uuid::new_v5(&user_id, &name.as_bytes()),
            user_id,
            name,
        }
    }
}
