FROM postgres:latest
WORKDIR /usr/local/bin/
ADD static static/
ADD target/release/yapft ./
ENTRYPOINT ["/usr/local/bin/yapft"]
