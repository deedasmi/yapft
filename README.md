# yapft
[![pipeline status](https://gitlab.com/deedasmi/yapft/badges/master/pipeline.svg)](https://gitlab.com/deedasmi/yapft/commits/master) 
[![coverage report](https://gitlab.com/deedasmi/yapft/badges/master/coverage.svg)](https://gitlab.com/deedasmi/yapft/commits/master)

Yet Another Personal Finance Tool

THIS IS NOT FREE (speech) OR OPEN SOURCE SOFTWARE!

This project uses the CC-BY-NC-SA license. Information can be found [here](https://creativecommons.org/licenses/by-nc-sa/4.0/).

The short version is that you are free to host this tool, as-is or modified, 
for friends, family, or anyone else. However, you may not collect payment or 
otherwise profit (such as hosting advertisements) from doing so.

That said, if you accept the license, contributions are welcome.
